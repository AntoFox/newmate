#!/bin/bash
#-------------------------------------------------------------------------------------------
echo "questo script scarica il source code di MATE da branch master"
#-------------------------------------------------------------------------------------------
# prente tutti i repo di un utente da github
# con jq li filtra per nome
# la grep toglie quello che non mi serve
#-------------------------------------------------------------------------------------------
LISTA=$(curl -s "https://api.github.com/users/mate-desktop/repos?per_page=100&page=1"|\
                jq -r '.[] | .name'|\
                grep -Ev 'wiki|debian-packages|mate-desktop.org|theme|mate-dev-scripts')
#-------------------------------------------------------------------------------------------
# se la cartella newMate non esiste la crea
#-------------------------------------------------------------------------------------------
    if [ ! -d newMateGitHub ]; then
        echo "La cartella newMateGitHub NON esiste, la creo"
        mkdir newMateGitHub
        ls | grep --color newMateGitHub
    fi

cd newMateGitHub
#-------------------------------------------------------------------------------------------
# un ciclone che per ogni elemento di LISTA
# controlla se già esiste
# se non esiste lo clona
#-------------------------------------------------------------------------------------------
for package in ${LISTA[@]}
 do
        if [ -d $package ]; then
              echo "repo $package già clonato"
          else
              echo "cloning $package"
              git clone --quiet https://github.com/mate-desktop/$package
        fi
done


